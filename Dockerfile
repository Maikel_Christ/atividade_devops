FROM python
WORKDIR /app
COPY . .
EXPOSE 8080
CMD ["python"]